<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HaModel */

$this->title = 'Update Ha Model: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Ha Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ha-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
