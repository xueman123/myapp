<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HaModel */

$this->title = 'Create Ha Model';
$this->params['breadcrumbs'][] = ['label' => 'Ha Models', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ha-model-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
